<?php
if (isset($_POST['btn'])) {
    $result_cart_update = $obj_apps->update_cart_product($_POST);
}


if (isset($_GET['status'])) {
    $cart_id = $_GET['id'];
    $result_cart_delete = $obj_apps->delete_cart_product($cart_id);
}


$session = session_id();
$result = $obj_apps->select_cart_product_by_session_id($session);
?>



<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Shopping Cart</li>
                <h3><?php
                    if (isset($result_cart_update)) {
                        echo $result_cart_update;
                        unset($result_cart_update);
                    }
                    ?></h3>
                <h4> <?php
                    if (isset($result_cart_delete))
                        echo $result_cart_delete;
                    unset($result_cart_delete);
                    ?></h4>
            </ol>

        </div>

        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Item</td>
                        <td class="description"></td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sum = 0;
                    while ($resul = mysqli_fetch_assoc($result)) {
                        ?>
                        <tr>
                            <td class="cart_product">
                                <a href=""><img src="assets/<?php echo $resul['product_image']; ?>" alt="" width="100" height="100"></a>
                            </td>
                            <td class="cart_description">
                                <h4><a href=""><?php echo $resul['product_name']; ?></a></h4>
                                <p><?php echo"821090" . $resul['product_id']; ?></p>
                            </td>
                            <td class="cart_price">
                                <p><?php echo "BDT: " . $resul['product_price']; ?></p>
                            </td>
                            <td class="cart_quantity">
                                <form action="" method="post">
                                    <div class="cart_quantity_button">
                                        <input class="cart_quantity_input" type="text" name="product_quentiity" value="<?php echo $resul['product_quentiity']; ?>" autocomplete="off" size="2">
                                        <input type="hidden" class="cart_quantity_input" name="tem_id" value="<?php echo $resul['tem_id']; ?>">
                                        <input type="submit" name="btn"class='cart_quantity_down' value="update">
                                    </div>
                                </form>
                            </td>
                            <td class="cart_total">
                                <p class="cart_total_price">
                                    <?php
                                    $total = $resul['product_quentiity'] * $resul['product_price'];
                                    echo "BDT: " . $total;
                                    ?>
                                </p>
                            </td>
                            <td class="cart_delete">
                                <a class="cart_quantity_delete" href="?status=delete&&id=<?php echo $resul['tem_id'] ?>"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>


                        <?php
                        $sum = $sum + $total;
                    }
                    ?>
                </tbody>
            </table>
            <table class="table table-bordered" style="width: 60%;float: right">
                <tr>
                    <td>Subtotal</td>
                    <td>BDT: <?php echo $sum; ?></td>
                </tr>
                <tr>
                    <td>Vat Total: </td>
                    <td>BDT: <?php
                        $vat = ($sum * 10) / 100;
                        echo $vat;
                        ?></td>
                </tr>
                <tr>
                    <td>Total</td>
                    <td>BDT: <?php
                        $grandTotal = $sum + $vat;
                        $_SESSION['orderTotal'] = $grandTotal;
                        echo $grandTotal;
                        ?></td>
                </tr>
            </table>
        </div>
    </div>
</section> <!--/#cart_items-->

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <?php
                    $customerId = isset($_SESSION['customerId']);
                    $shippingId = isset($_SESSION['shippingId']);
                    if ($customerId == NULL && $shippingId == NULL) {
                        ?>
                        <a href="checkout.php" class="btn btn-primary pull-right">Checkout</a>
                    <?php } elseif ($customerId != NULL && $shippingId == NULL) { ?>
                        <a href="shipping.php" class="btn btn-primary pull-right">Checkout</a>
                    <?php } elseif ($customerId != NULL && $shippingId != NULL) { ?>
                        <a href="payment.php" class="btn btn-primary pull-right">Checkout</a>
<?php } ?>
                    <a href="index.php" class="btn btn-primary pull-left">Continue Shipping</a>
                </div>
            </div>
        </div>
    </div>
</div>