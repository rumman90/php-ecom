<?php
$customerId = $_SESSION['customerId'];

$result=$obj_apps->select_customer_info($customerId);
$customerInfo= mysqli_fetch_assoc($result);

if(isset($_POST['btnShipping'])){
    $obj_apps->save_shipping_info($_POST);
}
?>


<div class="container">

    <div class="row ">
        <div class="col-sm-12 pull-left">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="text-center text-success">Shipping Point here</h2>
                    <form class="form-horizontal" action="" method="post">
                        <div class="form-group">
                            <label class="control-label col-lg-3">First Name</label>
                            <div class="col-lg-9">
                                <input type="text" name="firstName"  value="<?php echo $customerInfo['cusName']?>"required class="form-control"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">Email</label>
                            <div class="col-lg-9">
                                <input type="text" name="email" value="<?php echo $customerInfo['cusEmail']?>" required class="form-control"> 
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label class="control-label col-lg-3">Contact</label>
                            <div class="col-lg-9">
                                <input type="number" name="contact" value="<?php echo $customerInfo['cusContact']?>" required class="form-control"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">Address</label>
                            <div class="col-lg-9">
                                <input type="text" name="address"  value="<?php echo $customerInfo['cusAddress']?>" required class="form-control"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">city</label>
                            <div class="col-lg-9">
                                <input type="text" name="city"  value="<?php echo $customerInfo['cusCity']?>" required class="form-control"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">District</label>
                            <div class="col-lg-9">
                                <input type="text" name="district"  value="<?php echo $customerInfo['cusDistrict']?>" required class="form-control"> 
                            </div>
                        </div>  
                        <input type="submit" name="btnShipping" value="Save Shipping  " class="btn btn-primary pull-right"> 
                    </form>
                </div>
            </div>
        </div>


    </div>
</div>
