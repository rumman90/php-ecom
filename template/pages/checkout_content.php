<?php
if(isset($_POST['signup'])){
    $obj_apps->save_customer_info($_POST);
}

?>

        <script type="text/javascript">

            function ajax_email_check(given_email,objID) {
                xmlhttp = new XMLHttpRequest();
                 //alert(objID);               
                serverPage = 'server.php?email='+given_email;
                xmlhttp.open("GET", serverPage);
                xmlhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById(objID).innerHTML = this.responseText;
                    }
                };
                xmlhttp.send();
            }
        </script>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="text-center text-success">You have to Login To complete your valuable order</h2>

                </div>
            </div>
        </div>
    </div>
    <div class="row ">
        <div class="col-sm-6 pull-left">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="text-center text-success">Registration From</h2>
                    <form class="form-horizontal" action="" method="post">
                        <div class="form-group">
                            <label class="control-label col-lg-3">First Name</label>
                            <div class="col-lg-9">
                                <input type="text" name="firstName" required class="form-control"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">Email</label>
                            <div class="col-lg-9">
                                <input type="text" name="email" required class="form-control" onblur="ajax_email_check(this.value,'res');"> 
                                <span id="res"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">password</label>
                            <div class="col-lg-9">
                                <input type="text" name="password" required class="form-control"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">Contact</label>
                            <div class="col-lg-9">
                                <input type="number" name="contact" required class="form-control"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">Address</label>
                            <div class="col-lg-9">
                                <input type="text" name="address" required class="form-control"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">city</label>
                            <div class="col-lg-9">
                                <input type="text" name="city" required class="form-control"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">District</label>
                            <div class="col-lg-9">
                                <input type="text" name="district" required class="form-control"> 
                            </div>
                        </div>  
                        <input type="submit" name="signup" value="Registration " class="btn btn-primary pull-right"> 
                    </form>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="text-center text-success">Login From</h2>
                    <form class="form-horizontal" action="" method="post">

                        <div class="form-group">
                            <label class="control-label col-lg-3">Email</label>
                            <div class="col-lg-9">
                                <input type="text" name="email" required class="form-control"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">password</label>
                            <div class="col-lg-9">
                                <input type="text" name="password" required class="form-control"> 
                            </div>
                        </div>


                        <input type="submit" name="btn" value="Login " class="btn btn-primary pull-right"> 
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
