<?php
$customerId = $_SESSION['customerId'];


if(isset($_POST['btn'])){
    $obj_apps->save_order_info($_POST);
}
?>

<div class="container">

    <div class="row ">
        <div class="col-sm-12 pull-left">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="text-center text-success">Select Your Payment Method</h2>
                    <form class="form-horizontal" action="" method="post">
                        <table class="table table-bordered">
                            <tr>
                                <td>Catch ON delivery</td>
                                <td><input type="radio" name="payment_type" value="cashonDelivery"></td>
                            </tr>
                            <tr>
                                <td>Paypal</td>
                                <td><input type="radio" name="payment_type" value="paypal"></td>
                            </tr>
                            <tr>
                                <td>Bkash</td>
                                <td><input type="radio" name="payment_type" value="Bkash"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td ><input type="submit" name="btn" value="Confirm Order" class="btn btn-primary"></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
