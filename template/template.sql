-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 19, 2018 at 12:56 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `template`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `cusId` int(10) NOT NULL,
  `cusName` varchar(50) NOT NULL,
  `cusEmail` varchar(50) NOT NULL,
  `cusPassword` varchar(50) NOT NULL,
  `cusContact` int(20) NOT NULL,
  `cusAddress` text NOT NULL,
  `cusCity` varchar(50) NOT NULL,
  `cusDistrict` varchar(50) NOT NULL,
  `activation_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`cusId`, `cusName`, `cusEmail`, `cusPassword`, `cusContact`, `cusAddress`, `cusCity`, `cusDistrict`, `activation_status`) VALUES
(1, 'rumman', 'r@gmail.com', '12345', 123456789, 'dhaka', 'dhaka', 'dhaka', 0),
(2, 'rumman', 'r@gmail.com', '123456', 12, 'dhaka', 'dhaka', 'dhaka', 0),
(3, 'rana', 'rana@gmail.com', '12345', 12345, 'dhaka', 'dhaka', 'dhaka', 0),
(4, 'samanta', 's@gmail.com', '12345', 3333, 'ad', 'dhaka', 'asdfds', 0),
(5, 'samanta', 's@gmail.com', '12345', 3333, 'ad', 'dhaka', 'asdfds', 0),
(6, 'samanta', 's@gmail.com', '12345', 3333, 'ad', 'dhaka', 'asdfds', 0),
(7, 'samanta', 's@gmail.com', '12345', 3333, 'ad', 'dhaka', 'asdfds', 0),
(8, 'samanta', 's@gmail.com', '12345', 3333, 'ad', 'dhaka', 'asdfds', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `db_id` int(10) NOT NULL,
  `db_name` varchar(50) NOT NULL,
  `db_email` varchar(50) NOT NULL,
  `db_password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`db_id`, `db_name`, `db_email`, `db_password`) VALUES
(1, 'admin', 'admin@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `category_id` int(10) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `category_description` text NOT NULL,
  `publication_status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`category_id`, `category_name`, `category_description`, `publication_status`) VALUES
(8, 'Man Fashion', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Man Fashion</span></font>', '1'),
(9, 'Kids  Fashion', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Kids &nbsp;Fashion</span></font>', '1'),
(10, 'Woman Fashion', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Woman Fashion</span></font>', '1'),
(11, 'Smart phone', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Smart phone</span></font>', '1'),
(13, 'mazhar', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_manu`
--

CREATE TABLE `tbl_manu` (
  `d_manu_id` int(11) NOT NULL,
  `d_manu_name` varchar(100) NOT NULL,
  `d_manu_description` text NOT NULL,
  `d_publication_status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_manu`
--

INSERT INTO `tbl_manu` (`d_manu_id`, `d_manu_name`, `d_manu_description`, `d_publication_status`) VALUES
(4, 'crime', 'cccc', '1'),
(6, 'Sony', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Sony</span></font>', '1'),
(7, 'Samsung', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Samsung</span></font>', '1'),
(8, 'Symphony', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Symphony</span></font>', '1'),
(9, 'arong', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">arong</span></font>', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `order_id` int(20) NOT NULL,
  `customer_id` varchar(20) NOT NULL,
  `shipping_id` int(20) NOT NULL,
  `order_total` float NOT NULL,
  `order_status` varchar(25) NOT NULL DEFAULT 'pending',
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`order_id`, `customer_id`, `shipping_id`, `order_total`, `order_status`, `order_date`) VALUES
(1, '3', 2, 2200, 'pending', '2018-10-18 19:48:04'),
(2, '3', 2, 1320, 'pending', '2018-10-18 19:51:20'),
(3, '8', 3, 4070, 'pending', '2018-10-18 20:59:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_details`
--

CREATE TABLE `tbl_order_details` (
  `order_details_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_price` float NOT NULL,
  `product_quentity` int(10) NOT NULL,
  `product_image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_order_details`
--

INSERT INTO `tbl_order_details` (`order_details_id`, `order_id`, `product_id`, `product_name`, `product_price`, `product_quentity`, `product_image`) VALUES
(1, 1, 6, 'i LiKe tHaT Shirt', 2000, 1, '../assets/product_image/vv.jpg'),
(2, 2, 5, 'color t shart', 1200, 1, '../assets/product_image/1.jpg'),
(3, 3, 5, 'color t shart', 1200, 1, '../assets/product_image/1.jpg'),
(4, 3, 4, 'polo t shart', 2500, 1, '../assets/product_image/sports-dry-fit-tshirt-250x250.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE `tbl_payment` (
  `payment_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `payment_type` varchar(30) NOT NULL,
  `payment_status` varchar(25) NOT NULL DEFAULT 'pending',
  `payment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_payment`
--

INSERT INTO `tbl_payment` (`payment_id`, `order_id`, `payment_type`, `payment_status`, `payment_date`) VALUES
(1, 1, 'cashonDelivery', 'pending', '2018-10-18 19:48:04'),
(2, 2, 'cashonDelivery', 'pending', '2018-10-18 19:51:20'),
(3, 3, 'cashonDelivery', 'pending', '2018-10-18 20:59:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(256) NOT NULL,
  `category_id` int(3) NOT NULL,
  `d_menu_id` int(3) NOT NULL,
  `product_price` float(10,2) NOT NULL,
  `stock_amount` int(7) NOT NULL,
  `minimum_stock_amount` varchar(5) NOT NULL,
  `product_short_discription` text NOT NULL,
  `product_long_discription` text NOT NULL,
  `product_image` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`product_id`, `product_name`, `category_id`, `d_menu_id`, `product_price`, `stock_amount`, `minimum_stock_amount`, `product_short_discription`, `product_long_discription`, `product_image`, `publication_status`) VALUES
(1, 'T-shart', 8, 9, 1600.00, 50, '5', 'well product .<span style=\"font-size: 13.3333px;\">well product.&nbsp;</span><span style=\"font-size: 13.3333px;\">well product .</span><span style=\"font-size: 13.3333px;\">well product .</span><span style=\"font-size: 13.3333px;\">well product.&nbsp;</span><span style=\"font-size: 13.3333px;\">well product</span>', '<span style=\"color: rgb(51, 51, 51); font-family: Nunito, serif; font-size: 15.5px; background-color: rgb(255, 255, 255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>', '../assets/product_image/5a38f36dc8588236bcc9575602e4464b.jpg', 1),
(4, 'polo t shart', 8, 9, 2500.00, 500, '5', 'goood product', '<span style=\"font-size: 13.3333px;\">goood product&nbsp;</span><span style=\"font-size: 13.3333px;\">goood product&nbsp;</span><span style=\"font-size: 13.3333px;\">goood product&nbsp;</span><span style=\"font-size: 13.3333px;\">goood product&nbsp;</span><span style=\"font-size: 13.3333px;\">goood product&nbsp;</span><span style=\"font-size: 13.3333px;\">goood product&nbsp;</span><span style=\"font-size: 13.3333px;\">goood product&nbsp;</span><span style=\"font-size: 13.3333px;\">goood product&nbsp;</span><span style=\"font-size: 13.3333px;\">goood product&nbsp;</span><span style=\"font-size: 13.3333px;\">goood product&nbsp;</span><span style=\"font-size: 13.3333px;\">goood product&nbsp;</span><span style=\"font-size: 13.3333px;\">goood product</span>', '../assets/product_image/sports-dry-fit-tshirt-250x250.jpg', 1),
(5, 'color t shart', 8, 9, 1200.00, 100, '5', 'color t shart', '<span style=\"font-size: 13.3333px;\">color t shart&nbsp;</span><span style=\"font-size: 13.3333px;\">color t shart&nbsp;</span><span style=\"font-size: 13.3333px;\">color t shart&nbsp;</span><span style=\"font-size: 13.3333px;\">color t shart&nbsp;</span><span style=\"font-size: 13.3333px;\">color t shart</span>', '../assets/product_image/1.jpg', 1),
(6, 'i LiKe tHaT Shirt', 8, 9, 2000.00, 200, '10', '<div style=\"box-sizing: inherit; color: rgb(33, 32, 50); font-family: &quot;Mr Eaves XL Modern&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 16px; background-color: rgb(255, 255, 255);\"><b style=\"box-sizing: inherit;\">Screw The Map Youll Be Alright<br style=\"box-sizing: inherit;\"></b></div><br>', '<span style=\"color: rgb(33, 32, 50); font-family: &quot;Mr Eaves XL Modern&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 16px; background-color: rgb(255, 255, 255);\">Only available for a LIMITED TIME, so get yours TODAY</span><div style=\"box-sizing: inherit; color: rgb(33, 32, 50); font-family: &quot;Mr Eaves XL Modern&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 16px; background-color: rgb(255, 255, 255);\">Choose your Styles, Size, Color then Click Add To Cart\"to place your&nbsp;</div>', '../assets/product_image/vv.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shipping`
--

CREATE TABLE `tbl_shipping` (
  `shi_id` int(11) NOT NULL,
  `shi_Name` varchar(50) NOT NULL,
  `shi_Email` varchar(50) NOT NULL,
  `shi_contact` varchar(50) NOT NULL,
  `shi_address` text NOT NULL,
  `shi_city` varchar(50) NOT NULL,
  `shi_district` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_shipping`
--

INSERT INTO `tbl_shipping` (`shi_id`, `shi_Name`, `shi_Email`, `shi_contact`, `shi_address`, `shi_city`, `shi_district`) VALUES
(1, 'rumman', 'r@gmail.com', '12', 'dhaka', 'dhaka', 'dhaka'),
(2, 'rana', 'rana@gmail.com', '12345', 'dhaka', 'dhaka', 'dhaka'),
(3, 'samanta', 's@gmail.com', '3333', 'ad', 'dhaka', 'asdfds');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tem_cart`
--

CREATE TABLE `tbl_tem_cart` (
  `tem_id` int(11) NOT NULL,
  `session_id` varchar(256) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(256) NOT NULL,
  `product_price` float(10,2) NOT NULL,
  `product_quentiity` int(7) NOT NULL,
  `product_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_tem_cart`
--

INSERT INTO `tbl_tem_cart` (`tem_id`, `session_id`, `product_id`, `product_name`, `product_price`, `product_quentiity`, `product_image`) VALUES
(7, 'mc4tr1l2nsqsdpcsikt39ibn9n', 6, 'i LiKe tHaT Shirt', 2000.00, 1, '../assets/product_image/vv.jpg'),
(12, 'gr6ehvn7pllj5t5geufhurakq4', 5, 'color t shart', 1200.00, 1, '../assets/product_image/1.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`cusId`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`db_id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tbl_manu`
--
ALTER TABLE `tbl_manu`
  ADD PRIMARY KEY (`d_manu_id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
  ADD PRIMARY KEY (`order_details_id`);

--
-- Indexes for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_shipping`
--
ALTER TABLE `tbl_shipping`
  ADD PRIMARY KEY (`shi_id`);

--
-- Indexes for table `tbl_tem_cart`
--
ALTER TABLE `tbl_tem_cart`
  ADD PRIMARY KEY (`tem_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `cusId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `db_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_manu`
--
ALTER TABLE `tbl_manu`
  MODIFY `d_manu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `order_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
  MODIFY `order_details_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_shipping`
--
ALTER TABLE `tbl_shipping`
  MODIFY `shi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_tem_cart`
--
ALTER TABLE `tbl_tem_cart`
  MODIFY `tem_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
