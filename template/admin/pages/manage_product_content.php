<?php
//this function for update or change catagories

if (isset($_GET['status'])) {
    $product_id = $_GET['id'];
    if ($_GET['status'] == "unpublished") {
        $view_result = $obj_super_admin->unpublished_product_by_id($product_id);
    } else if ($_GET['status'] == "published") {
        $view_result = $obj_super_admin->published_product_by_id($product_id);
    } else if ($_GET['status'] == "delete") {
        $view_result = $obj_super_admin->delete_product_by_id($product_id);
    }
}

//end

$final_product_result = $obj_super_admin->view_product();
?>

<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white user"></i><span class="break"></span>Members</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <h2> <?php if (isset($view_result)) echo $view_result;
unset($view_result); ?></h2>
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Product name</th>
                        <th>Category name</th>
                        <th>Manufacture name</th>
                        <th>Product Price</th>
                        <th>Stock Amount</th>
                        <th>Publication Status</th>
                        <th>Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php while ($result = mysqli_fetch_assoc($final_product_result)) { ?>
                        <tr>
                            <td class="center"><?php echo $result['product_name']; ?></td>
                            <td class="center"><?php echo $result['category_name']; ?></td>
                            <td class="center"><?php echo $result['d_manu_name']; ?></td>
                            <td class="center"><?php echo $result['product_price']; ?></td>
                            <td class="center"><?php echo $result['stock_amount']; ?></td>
                            <td class="center">

                                    <?php
                                    if ($result['publication_status'] == 1) {
                                        echo "published";
                                    } else {
                                        echo "unpublished";
                                    }
                                    ?>
                            </td>
                            <td class="center">
                               
                                <a class="btn btn-primary" href="view_all_product.php?idsend=<?php echo $result['product_id']; ?>" title="view all">
                                        <i class="halflings-icon white zoom-in"></i>  
                                    </a> 

                                    <?php
// thsi functio for publish unpubish
                                    if ($result['publication_status'] == 1) {
                                        ?>
                                        <a class="btn btn-success" href="?status=unpublished&&id=<?php echo $result['product_id']; ?>" title="unpublished">
                                            <i class="halflings-icon white arrow-down"></i>  
                                        </a>
                                    <?php } else { ?>
                                        <a class="btn btn-danger" href="?status=published&&id=<?php echo $result['product_id']; ?>" title="published">
                                            <i class="halflings-icon white arrow-up"></i>  
                                        </a>
                                    <?php } ?>
                                    <a class="btn btn-info" href="edit_product.php?idsend=<?php echo $result['product_id']; ?>">
                                        <i class="halflings-icon white edit"></i>  
                                    </a>
                                    <a class="btn btn-danger" href="?status=delete&&id=<?php echo $result['product_id']; ?> ">
                                        <i class="halflings-icon white trash"></i> 
                                </a>
                            </td>
                        </tr>

                    <?php } ?>
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->