<?php
$order_id=$_GET['idsend'];
$Customer_query_result=$obj_super_admin->select_customer_info_By_order_id($order_id);
$customer=mysqli_fetch_assoc($Customer_query_result);

$shipping_query_result=$obj_super_admin->select_shipping_info_By_order_id($order_id);
$shipping=mysqli_fetch_assoc($shipping_query_result);

$payment_query_result=$obj_super_admin->select_payment_info_By_order_id($order_id);
$payment=mysqli_fetch_assoc($payment_query_result);
?>



<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white user"></i><span class="break"></span>Members</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <h2> <?php
                if (isset($view_result))
                    echo $view_result;
                unset($view_result);
                ?></h2>
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <h2>Customer Info</h2>
                <tr>
                    <td>Customer  Name</td> 
                    <td><?php echo $customer['cusName']?></td>
                </tr>
                <tr>
                    <td>Customer  Email</td> 
                    <td><?php echo $customer['cusEmail']?></td>
                </tr>
                <tr>
                    <td>phone</td> 
                    <td><?php echo $customer['cusContact']?></td>
                </tr>
                <tr>
                    <td>Address</td> 
                    <td><?php echo $customer['cusAddress']?></td>
                </tr>
                <tr>
                    <td>City</td> 
                    <td><?php echo $customer['cusCity']?></td>
                </tr>
                <tr>
                    <td>District</td> 
                    <td><?php echo $customer['cusDistrict']?></td>
                </tr>
            </table>    
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <h2>Shipping Info</h2>
                <tr>
                    <td>Shipping  Name</td> 
                    <td><?php echo $shipping['shi_Name']?></td>
                </tr>
                <tr>
                    <td>Shipping  Email</td> 
                    <td><?php echo $shipping['shi_Email']?></td>
                </tr>
                <tr>
                    <td>phone</td> 
                    <td><?php echo $shipping['shi_contact']?></td>
                </tr>
                <tr>
                    <td>Address</td> 
                    <td><?php echo $shipping['shi_address']?></td>
                </tr>
                <tr>
                    <td>City</td> 
                    <td><?php echo $shipping['shi_city']?></td>
                </tr>
                <tr>
                    <td>District</td> 
                    <td><?php echo $shipping['shi_district']?></td>
                </tr>
            </table>    
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <h2>Payment Info</h2>
                <tr>
                    <td>payment type </td> 
                    <td><?php echo $payment['payment_type']?></td>
                </tr>
                <tr>
                    <td>payment  Status</td> 
                    <td><?php echo $payment['payment_status']?></td>
                </tr>

            </table>  
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <h2>Product Info</h2>
                <tr>
                    <td>Product Id </td> 
                    <td>Product Name</td>
                    <td>Product Image</td>
                    <td>Product Price</td>
                    <td>Product Quentity</td>
                    <td>Total Price</td>
                </tr>
                <tr>
                    <td>Product Id </td> 
                    <td>Product Name</td>
                    <td>Product Image</td>
                    <td>Product Price</td>
                    <td>Product Quentity</td>
                    <td>Total Price</td>
                </tr>

            </table>  
        </div>
    </div><!--/span-->

</div>