<?php
require '../class/application.php'; // orthat main file holo admin master sekhan theke ak ghor bahire 
$obj_app = new Application();

$category_query_result = $obj_app->select_all_published_category();
$manu_query_result = $obj_app->select_all_published_manufacture();

if (isset($_POST['btn'])) {
     $final_massage=$obj_super_admin->add_product_info($_POST);
}
?>



<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white edit"></i><span class="break"></span>Add product Form </h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
            </div>
        </div>

        <h2 style="color:green; text-align:center;"> <?php if(isset($final_massage)) echo  $final_massage;?></h2>



        <div class="box-content">
            <form action="" method="POST" class="form-horizontal" enctype="multipart/form-data">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Product  Name  </label>
                        <div class="controls">
                            <input type="text" name="f_product_name" class="span6 typeahead" id="typeahead">

                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label" for="date01">Category Name</label>
                        <div class="controls">
                            <select name="category_id" id="selectError3">// eta holo add product table er name

                                <option>---Select Category Name ---</option>
                                <?php while ($category_result = mysqli_fetch_assoc($category_query_result)) { ?>
                                    <option value="<?php echo $category_result['category_id']; ?>"> <?php echo $category_result['category_name']; ?></option>
                                <?php } ?>

                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="date01">Manufacturer Name</label>
                        <div class="controls">
                            <select name="d_menu_id" id="selectError3">   // eta holo add product table er name

                                <option>---Select Manufacturer Name ---</option>
                                <?php while ($manu_result = mysqli_fetch_assoc($manu_query_result)) { ?>
                                    <option value="<?php echo $manu_result["d_manu_id"]; ?> "> <?php echo $manu_result["d_manu_name"]; ?> </option>
                                <?php } ?>

                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="typeahead">Product  Price  </label>
                        <div class="controls">
                            <input type="number" name="f_product_price" class="span6 typeahead" id="typeahead">

                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="typeahead">	Stock Amount </label>
                        <div class="controls">
                            <input type="number" name="f_stock_amount" class="span6 typeahead" id="typeahead">

                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="typeahead">	Minimum Stock Amount </label>
                        <div class="controls">
                            <input type="number" name="f_minimum_stock" class="span6 typeahead" id="typeahead">

                        </div>
                    </div>



                    <div class="control-group hidden-phone">
                        <label class="control-label" for="textarea2">Product short Description</label>
                        <div class="controls">
                            <textarea class="cleditor" name="f_product_short"id="textarea2" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="control-group hidden-phone">
                        <label class="control-label" for="textarea2">Product long Description</label>
                        <div class="controls">
                            <textarea class="cleditor" name="f_product_long"id="textarea2" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="typeahead">	Product Image </label>
                        <div class="controls">
                            <input type="file" name="f_product_image" class="span6 typeahead" id="typeahead">

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="date01">Publication Status </label>
                        <div class="controls">
                            <select name="f_publication_status" id="selectError3">

                                <option>---Select Publication Status ---</option>
                                <option value="1"> Publish </option>
                                <option value="0"> Unpublish </option>

                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" name="btn" class="btn btn-primary">Save Product Information </button>
                        <button type="reset" class="btn">Reset</button>
                    </div>
                </fieldset> 
            </form>   

        </div>
    </div><!--/span-->

</div>