<?php
$product_id=$_GET['idsend'];

$query_result=$obj_super_admin->select_all_product_info($product_id);
$product_info=mysqli_fetch_assoc($query_result);// ekta array asteche tai loop lagbe na
?>


<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white user"></i><span class="break"></span>View All Product</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
            </div>
        </div>
        <div class="box-content">
        
            <table class="table table-striped table-bordered bootstrap-datatable datatable">

                <tr>
                    <th>Product Id</th>
                    <th><?php echo $product_info['product_id']; ?></th>
                </tr> 
                <tr>
                    <th>Product Name</th>
                    <th><?php echo $product_info['product_name']; ?></th>
                </tr> 
                <tr>
                    <th>Category Id </th>
                    <th><?php echo $product_info['category_id']; ?></th>
                </tr> 
                <tr>
                    <th>Manufacture id</th>
                    <th><?php echo $product_info['d_menu_id']; ?></th>
                </tr> 
                <tr> 
                    <th>Product Price </th>
                    <th><?php echo $product_info['product_price']; ?></th>
                </tr> 
                <tr>
                    <th>Stock amount</th>
                    <th><?php echo $product_info['stock_amount']; ?></th>
                </tr> 
                <tr>    
                    <th>MIn Stock Amount</th>
                    <th><?php echo $product_info['minimum_stock_amount']; ?></th>     
                </tr> 
                <tr>
                    <th>Product Short Desc; </th>
                    <th><?php echo $product_info['product_short_discription']; ?></th>
                </tr> 
                <tr>
                    <th>Product Long Desc; </th>
                    <th><?php echo $product_info['product_long_discription']; ?></th>
                </tr> 
                <tr>
                    <th>Product Image </th>
                    <th>   
                        <img src="<?php echo $product_info['product_image']; ?>" alt="<?php echo $product_info['product_name']; ?>" width="150" height="150" />
                    </th>
                </tr> 
                <tr>
                    <th>Publication Status </th>
                    <th><?php
                        if ($product_info['publication_status'] == 1) {
                            echo "published";
                        } else {
                            echo "unpublished";
                        }
                        ?>
                    </th>
                </tr> 
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->