<?php
//this function for update or change catagories
//if (isset($_GET['status'])) {
//    $product_id = $_GET['id'];
//    if ($_GET['status'] == "unpublished") {
//        $view_result = $obj_super_admin->unpublished_product_by_id($product_id);
//    } else if ($_GET['status'] == "published") {
//        $view_result = $obj_super_admin->published_product_by_id($product_id);
//    } else if ($_GET['status'] == "delete") {
//        $view_result = $obj_super_admin->delete_product_by_id($product_id);
//    }
//}
//end

$order_result = $obj_super_admin->select_all_order_info();
?>

<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white user"></i><span class="break"></span>Members</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <h2> <?php
                if (isset($view_result))
                    echo $view_result;
                unset($view_result);
                ?></h2>
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Order Id</th>
                        <th>Customer name</th>
                        <th>Order Total</th>
                        <th>Order Status</th>
                        <th>Payment Type</th>
                        <th>Payment Status</th>
                        <th>Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php while ($result = mysqli_fetch_assoc($order_result)) { ?>
                        <tr>
                            <td class="center"><?php echo $result['order_id']; ?></td>
                            <td class="center"><?php echo $result['cusName']; ?></td>
                            <td class="center"><?php echo $result['order_total']; ?></td>
                            <td class="center"><?php echo $result['order_status']; ?></td>
                            <td class="center"><?php echo $result['payment_type']; ?></td>
                            <td class="center"><?php echo $result['payment_status']; ?></td>

                            <td class="center">

                                <a class="btn btn-primary" href="viewOrder.php?idsend=<?php echo $result['order_id']; ?>" title="view Order">
                                    <i class="halflings-icon white zoom-in"></i>  
                                </a> 
                                <a class="btn btn-primary" href="viewinVoice.php?idsend=<?php echo $result['order_id']; ?>" title="view Invoice">
                                    <i class="halflings-icon white zoom-in"></i>  
                                </a> 


                                <a class="btn btn-success" href="?status=unpublished&&id=<?php echo $result['order_id']; ?>" title="Download Invoice">
                                    <i class="halflings-icon white download"></i>  
                                </a>


                                <a class="btn btn-info" href="edit_product.php?idsend=<?php echo $result['order_id']; ?>" title="Edit Order">
                                    <i class="halflings-icon white edit"></i>  
                                </a>
                                <a class="btn btn-danger" href="?status=delete&&id=<?php echo $result['order_id']; ?> ">
                                    <i class="halflings-icon white trash"></i> 
                                </a>
                            </td>
                        </tr>

                    <?php } ?>
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->