<?php

//this function for update or change catagories

if(isset($_GET['status'])){
    $manufacture_id=$_GET['id'];
    if($_GET['status']=="unpublished"){
       $view_result= $obj_super_admin->unpublished_manufac_by_id($manufacture_id);
    }else if($_GET['status']=="published"){
         $view_result= $obj_super_admin->published_manufac_by_id($manufacture_id);
    }
    else if($_GET['status']=="delete"){
         $view_result= $obj_super_admin->delete_manufac_by_id($manufacture_id);
    }
}

//end

$final_manufacture_result = $obj_super_admin->view_manufacture();
?>

<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white user"></i><span class="break"></span>Members</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <h2> <?php if(isset($view_result)) echo $view_result; unset ($view_result); ?></h2>
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php while ($result = mysqli_fetch_assoc($final_manufacture_result)) { ?>
                        <tr>
                            <td><?php echo $result['d_manu_id']; ?></td>
                            <td class="center"><?php echo $result['d_manu_name']; ?></td>
                            <td class="center"><?php echo $result['d_manu_description']; ?></td>
                            <td class="center">
                                <span class="label label-success"><?php echo $result['d_publication_status']; ?></span>
                            </td>
                            <td class="center">
                                <?php
                                // thsi functio for publish unpubish
                                if ($result['d_publication_status'] == 1) {
                                  ?>
                                    <a class="btn btn-success" href="?status=unpublished&&id=<?php echo $result['d_manu_id'];?>" title="unpublished">
                                        <i class="halflings-icon white arrow-down"></i>  
                                    </a>
                              <?php } else { ?>
                                    <a class="btn btn-danger" href="?status=published&&id=<?php echo $result['d_manu_id'];?>" title="published">
                                        <i class="halflings-icon white arrow-up"></i>  
                                    </a>
    <?php } ?>
                                <a class="btn btn-info" href="edit_manufac.php?idsend=<?php echo $result['d_manu_id'];?>">
                                    <i class="halflings-icon white edit"></i>  
                                </a>
                                <a class="btn btn-danger" href="?status=delete&&id=<?php echo $result['d_manu_id'];?> ">
                                    <i class="halflings-icon white trash"></i> 
                                </a>
                            </td>
                        </tr>

<?php } ?>
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->