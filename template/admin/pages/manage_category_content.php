<?php

//this function for update or change catagories

if(isset($_GET['status'])){
    $catagori_id=$_GET['id'];
    if($_GET['status']=="unpublished"){
       $view_result= $obj_super_admin->unpublished_catagori_by_id($catagori_id);
    }else if($_GET['status']=="published"){
         $view_result= $obj_super_admin->published_catagori_by_id($catagori_id);
    }
    else if($_GET['status']=="delete"){
         $view_result= $obj_super_admin->delete_categori_by_id($catagori_id);
    }
}

//end

$final_category_result = $obj_super_admin->select_all_category_info();
?>

<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white user"></i><span class="break"></span>Members</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <h2> <?php if(isset($view_result)) echo $view_result; unset ($view_result); ?></h2>
           <h2>
 <?php if(isset($_SESSION['massage'])) echo $_SESSION['massage']; unset ($_SESSION['massage']); ?>
           </h2>
            
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php while ($result = mysqli_fetch_assoc($final_category_result)) { ?>
                        <tr>
                            <td><?php echo $result['category_id']; ?></td>
                            <td class="center"><?php echo $result['category_name']; ?></td>
                            <td class="center"><?php echo $result['category_description']; ?></td>
                            <td class="center">
                                <span class="label label-success"><?php echo $result['publication_status']; ?></span>
                            </td>
                            <td class="center">
                                <?php
                                // thsi functio for publish unpubish
                                if ($result['publication_status'] == 1) {
                                    ?>
                                    <a class="btn btn-success" href="?status=unpublished&&id=<?php echo $result['category_id'];?>" title="unpublished">
                                        <i class="halflings-icon white arrow-down"></i>  
                                    </a>
    <?php } else { ?>
                                    <a class="btn btn-danger" href="?status=published&&id=<?php echo $result['category_id'];?>" title="published">
                                        <i class="halflings-icon white arrow-up"></i>  
                                    </a>
    <?php } ?>
                                <a class="btn btn-info" href="edit_category.php?idsend=<?php echo $result['category_id'];?>" title="edit">
                                    <i class="halflings-icon white edit"></i>  
                                </a>
                                <a class="btn btn-danger" href="?status=delete&&id=<?php echo $result['category_id'];?> " title="delete" onclick="return check_delete();">
                                    <i class="halflings-icon white trash"></i> 
                                </a>
                            </td>
                        </tr>

<?php } ?>
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

</div><!--/row-->