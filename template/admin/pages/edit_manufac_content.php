<?php
// select from database updatae
$received_id = $_GET['idsend'];

$received = $obj_super_admin->select_info_from_database_manu($received_id); //parametter pass
$array_data_show = mysqli_fetch_assoc($received);


if (isset($_POST['btn'])) {
    $final_update = $obj_super_admin->update_information_manufac();
}
?>



<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white edit"></i><span class="break"></span> Update Or Edit Your Information </h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
            </div>
        </div>

  <h2 style="color:green; text-align:center;"><?php if(isset($final_update)) echo $final_update;
     
        ?></h2>


        <div class="box-content">
            <form action="" name="edit_manu_form" method="POST" class="form-horizontal">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Manufacture Name  </label>
                        <div class="controls">
                            <input type="text" name="manu_name" value="<?php echo $array_data_show['d_manu_name']; ?>" class="span6 typeahead" id="typeahead">
                            <input type="text" name="hidden_id" value="<?php echo $array_data_show['d_manu_id']; ?>" class="span6 typeahead" id="typeahead">

                        </div>
                    </div>


                    <div class="control-group hidden-phone">
                        <label class="control-label" for="textarea2">Manufacture Description</label>
                        <div class="controls">
                            <textarea class="cleditor" name="manu_description"id="textarea2" rows="3"><?php echo $array_data_show['d_manu_description']; ?>"</textarea>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="date01">Publication Status </label>
                        <div class="controls">
                            <select name="publication_status" id="selectError3">

                                <option>-------Select a Option --------</option>
                                <option value="1"> Publish </option>
                                <option value="0"> Unpublish </option>

                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" name="btn" class="btn btn-primary">update Manufacture informition </button>

                    </div>
                </fieldset> 
            </form>   

        </div>
    </div><!--/span-->

</div>

<!--- edit publication status-->
<script>
    document.forms['edit_manu_form'].elements['publication_status'].value='<?php echo $array_data_show['d_publication_status']; ?>';
</script>