<?php
       
        
if(isset($_POST['submit'])){
    $final_massage=$obj_super_admin->add_manufacture($_POST);
}
?>




<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon white edit"></i><span class="break"></span>Add A Manufacturer </h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
            </div>
        </div>
        
        <h2 style="color:green; text-align:center;"> <?php if(isset($final_massage)) echo  $final_massage;?></h2>
        
        
        <div class="box-content">
            <form action="" method="POST" class="form-horizontal">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Manufacturer Name  </label>
                        <div class="controls">
                            <input type="text" name="manu_name" class="span6 typeahead" id="typeahead">
                           
                        </div>
                    </div>
                    

                    <div class="control-group hidden-phone">
                        <label class="control-label" for="textarea2">Manufacturer Description</label>
                        <div class="controls">
                            <textarea class="cleditor" name="manu_description"id="textarea2" rows="3"></textarea>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="date01">Publication Status </label>
                        <div class="controls">
                            <select name="publication_status" id="selectError3">
                                
                                  <option>-------Select a Option --------</option>
                                  <option value="1"> Publish </option>
                                  <option value="0"> Unpublish </option>
                                  
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" name="submit" class="btn btn-primary">Save Manufacturer informition </button>
                        <button type="reset" class="btn">Reset</button>
                    </div>
                </fieldset> 
            </form>   

        </div>
    </div><!--/span-->

</div>