<div id="sidebar-left" class="span2">
    <div class="nav-collapse sidebar-nav">
        <ul class="nav nav-tabs nav-stacked main-menu">
            <li><a href="admin_master.php"><i class="icon-bar-chart"></i><span class="hidden-tablet"> Dashboard</span></a></li>	
            <li>
                <a style="background:#1C2B36" class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Category </span><span class="label label-important"> 3 </span></a>
                <ul>
                    <li><a href="category.php"><i class="icon-envelope"></i><span class="hidden-tablet"> Add Catagori </span></a></li>
                    <li><a href="manage_category.php"><i class="icon-tasks"></i><span class="hidden-tablet">Manage Catag:</span></a></li>       
                </ul>	
            </li>
            <li>
                <a style="background:#1C2B36" class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Manufactur </span><span class="label label-important"> 3 </span></a>
                <ul>
                    <li><a href="add_manufacture.php"><i class="icon-envelope"></i><span class="hidden-tablet"> Add Manuf: </span></a></li>
                    <li><a href="manage_manufacture.php"><i class="icon-tasks"></i><span class="hidden-tablet">Manage Manuf:</span></a></li>       
                </ul>	
            </li>
           
           
            <li><a href="add_product.php"><i class="icon-edit"></i><span class="hidden-tablet"> Add Product</span></a></li>
            <li><a href="manage_product.php"><i class="icon-list-alt"></i><span class="hidden-tablet"> Manage Product</span></a></li>
            <li><a href="manageOrder.php"><i class="icon-font"></i><span class="hidden-tablet"> Manage order</span></a></li>
            <li><a href="gallery.html"><i class="icon-picture"></i><span class="hidden-tablet"> Gallery</span></a></li>
            <li><a href="table.html"><i class="icon-align-justify"></i><span class="hidden-tablet"> Tables</span></a></li>
            <li><a href="calendar.html"><i class="icon-calendar"></i><span class="hidden-tablet"> Calendar</span></a></li>
            <li><a href="file-manager.html"><i class="icon-folder-open"></i><span class="hidden-tablet"> File Manager</span></a></li>
            <li><a href="icon.html"><i class="icon-star"></i><span class="hidden-tablet"> Icons</span></a></li>
            <li><a href="login.html"><i class="icon-lock"></i><span class="hidden-tablet"> Login Page</span></a></li>
        </ul>
    </div>
</div>