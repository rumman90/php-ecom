<?php
// same parent theke same cild ke session e hedder location diye pathale error tar jonno
ob_start();


session_start();   //for show author name
//this function is for  link ssecurity

$admin_id = $_SESSION['author_id'];   //this session is admin class variable session
if ($admin_id == NULL) {
    header('location:index.php');
}
//end


require 'class/super_admin.php';
$obj_super_admin = new Super_admin();

if (isset($_GET['status'])) {
    if ($_GET['status'] == 'logout') {
        $obj_super_admin->logout();
    }
}
?>




<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- start: Meta -->
        <meta charset="utf-8">
        <title>Bootstrap Metro Dashboard by Dennis Ji for ARM demo</title>
        <meta name="description" content="Bootstrap Metro Dashboard">
        <meta name="author" content="Dennis Ji">
        <meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
        <!-- end: Meta -->

        <!-- start: Mobile Specific -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- end: Mobile Specific -->

        <!-- start: CSS -->
        <link id="bootstrap-style" href="../assets/back_end_assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="../assets/back_end_assets/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link id="base-style" href="../assets/back_end_assets/css/style.css" rel="stylesheet">
        <link id="base-style-responsive" href="../assets/back_end_assets/css/style-responsive.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>

        <!-- start: Favicon -->
        <link rel="shortcut icon" href="../assets/back_end_assets/img/favicon.ico">


        <!-- function for confarm delete alert -->
        <script>
            function check_delete() {
                var check = confirm("Are you sure to delete this");
                if (check) {
                    return true;
                } else {
                    return false;
                }
            }

        </script>


    </head>

    <body>
        <!-- start: Header -->
        <?php include './include/header.php'; ?>
        <!-- end: Header -->

        <div class="container-fluid-full">
            <div class="row-fluid">

                <!-- start: Main Menu -->
                <?php include './include/sidebar_menu.php'; ?>       
                <!-- end: Main Menu -->

                <noscript>
                <div class="alert alert-block span10">
                    <h4 class="alert-heading">Warning!</h4>
                    <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
                </div>
                </noscript>

                <!-- start: Content -->
                <div id="content" class="span10">



                    <?php
                    if (isset($pages)) {
                        if ($pages == 'category') {
                            include './pages/category_content.php';
                        } elseif ($pages == "manage_category") {
                            include './pages/manage_category_content.php';
                        } elseif ($pages == "edit") {
                            include './pages/edit_category_content.php';
                        } elseif ($pages == "add_manu") {
                            include './pages/add_manufacture_content.php';
                        } elseif ($pages == "manage_manu") {
                            include './pages/manage_manufacture_content.php';
                        } elseif ($pages == "edit_manufac") {
                            include "./pages/edit_manufac_content.php";
                        } elseif ($pages == "add_product") {
                            include "./pages/add_product_content.php";
                        }elseif ($pages == "manage_product") {
                            include "./pages/manage_product_content.php";
                        }elseif ($pages == "view_all_product") {
                            include "./pages/view_all_product_content.php";
                        }elseif ($pages == "manage_order") {
                            include "./pages/manage_order_content.php";
                        }
                        elseif ($pages == "viewOrder") {
                            include "./pages/viewOrder_content.php";
                        }
                    } else {
                        include './pages/home_content.php';
                    }
                    ?>








                </div>
                <!--/.fluid-container-->

                <!-- end: Content -->
            </div><!--/#content.span10-->
        </div><!--/fluid-row-->

        <div class="modal hide fade" id="myModal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3>Settings</h3>
            </div>
            <div class="modal-body">
                <p>Here settings can be configured...</p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn" data-dismiss="modal">Close</a>
                <a href="#" class="btn btn-primary">Save changes</a>
            </div>
        </div>

        <div class="common-modal modal fade" id="common-Modal1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content">
                <ul class="list-inline item-details">
                    <li><a href="http://themifycloud.com">Admin templates</a></li>
                    <li><a href="http://themescloud.org">Bootstrap themes</a></li>
                </ul>
            </div>
        </div>

        <div class="clearfix"></div>

        <!-- footer part-->
<?php include './include/footer.php'; ?>

        <!-- start: JavaScript-->

        <script src="../assets/back_end_assets/js/jquery-1.9.1.min.js"></script>
        <script src="../assets/back_end_assets/js/jquery-migrate-1.0.0.min.js"></script>
        <script src="../assets/back_end_assets/js/jquery-ui-1.10.0.custom.min.js"></script>
        <script src="../assets/back_end_assets/js/jquery.ui.touch-punch.js"></script>
        <script src="../assets/back_end_assets/js/modernizr.js"></script>
        <script src="../assets/back_end_assets/js/bootstrap.min.js"></script>
        <script src="../assets/back_end_assets/js/jquery.cookie.js"></script>
        <script src='../assets/back_end_assets/js/fullcalendar.min.js'></script>
        <script src='../assets/back_end_assets/js/jquery.dataTables.min.js'></script>
        <script src="../assets/back_end_assets/js/excanvas.js"></script>
        <script src="../assets/back_end_assets/js/jquery.flot.js"></script>
        <script src="../assets/back_end_assets/js/jquery.flot.pie.js"></script>
        <script src="../assets/back_end_assets/js/jquery.flot.stack.js"></script>
        <script src="../assets/back_end_assets/js/jquery.flot.resize.min.js"></script>
        <script src="../assets/back_end_assets/js/jquery.chosen.min.js"></script>
        <script src="../assets/back_end_assets/js/jquery.uniform.min.js"></script>
        <script src="../assets/back_end_assets/js/jquery.cleditor.min.js"></script>
        <script src="../assets/back_end_assets/js/jquery.noty.js"></script>
        <script src="../assets/back_end_assets/js/jquery.elfinder.min.js"></script>
        <script src="../assets/back_end_assets/js/jquery.raty.min.js"></script>
        <script src="../assets/back_end_assets/js/jquery.iphone.toggle.js"></script>
        <script src="../assets/back_end_assets/js/jquery.uploadify-3.1.min.js"></script>
        <script src="../assets/back_end_assets/js/jquery.gritter.min.js"></script>
        <script src="../assets/back_end_assets/js/jquery.imagesloaded.js"></script>
        <script src="../assets/back_end_assets/js/jquery.masonry.min.js"></script>
        <script src="../assets/back_end_assets/js/jquery.knob.modified.js"></script>
        <script src="../assets/back_end_assets/js/jquery.sparkline.min.js"></script>
        <script src="../assets/back_end_assets/js/counter.js"></script>
        <script src="../assets/back_end_assets/js/retina.js"></script>
        <script src="../assets/back_end_assets/js/custom.js"></script>
        <!-- end: JavaScript-->

    </body>
</html>
