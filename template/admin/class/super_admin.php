<?php

/**
 * Description of super_admin
 *
 * @author rumman
 */
class Super_admin {

    //put your code here
    private $db_connect;

    //this is  a connection for catagorie page 
    public function __construct() {
        // just connect database 
        $host = 'localhost';
        $user = 'root';
        $password = '';
        $database = 'template';
        $this->db_connect = mysqli_connect($host, $user, $password, $database);
        if (!$this->db_connect) {
            die("Database Not Connect Successfully !!" . mysqli_error($this->db_connect));
        }
    }

    public function save_categor_info($data) {
        $query = "insert into tbl_category(category_name,category_description,publication_status) values('$data[category_name]','$data[category_description]','$data[publication_status]')";

        if (mysqli_query($this->db_connect, $query)) {
            $massage = "Congaratulation data Insert Succesfully  !!";
            return $massage;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
//       echo '<pre>';
//       print_r($data);
//        echo '</pre>';
    }

    // catagories add function exit 
    // this function is for view database data
    public function select_all_category_info() {
        $query = "select * from tbl_category";
        if (mysqli_query($this->db_connect, $query)) {
            $view_result = mysqli_query($this->db_connect, $query);
            return $view_result;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    //function for select info from database

    public function select_info_from_database($data) {
        $query = "select * from tbl_category where category_id='$data'";
        if (mysqli_query($this->db_connect, $query)) {
            $query_data = mysqli_query($this->db_connect, $query);
            return $query_data;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    // updatate function
    public function update_information() {
        $query = "update tbl_category set category_name='$_POST[category_name]' ,category_description='$_POST[category_description]',publication_status='$_POST[publication_status]' where category_id='$_POST[hidden_id]' ";
        if (mysqli_query($this->db_connect, $query)) {

            // sessoin e a ta massage rakhbo
            $_SESSION['massage'] = 'Oh congratulation !! Update data successfully';
            header('location:manage_category.php');
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

// this is for published unpublished 

    public function unpublished_catagori_by_id($catagori_id) {
        $query = "update tbl_category set publication_status='0' where category_id='$catagori_id' ";
        if (mysqli_query($this->db_connect, $query)) {
            $view_result = "catagori info unpublished successfully ";
            return $view_result;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    // unpublished

    public function published_catagori_by_id($catagori_id) {
        $query = "update tbl_category set publication_status='1' where category_id='$catagori_id' ";
        if (mysqli_query($this->db_connect, $query)) {
            $view_result = "catagori info published successfully ";
            return $view_result;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    //delete function 
    function delete_categori_by_id($data) {
        $query = "delete from tbl_category where category_id='$data' ";
        if (mysqli_query($this->db_connect, $query)) {
            $view_result = "delete information successfully ";
            return $view_result;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    // insert manufacture
    function add_manufacture($data) {
        $query = "insert into tbl_manu(d_manu_name,d_manu_description,d_publication_status) values('$data[manu_name]','$data[manu_description]','$data[publication_status]')";

        if (mysqli_query($this->db_connect, $query)) {
            $massage = "Congaratulation data Insert Succesfully  !!";
            return $massage;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    // view manufacture 
    // this function is for view database data
    public function view_manufacture() {
        $query = "select * from tbl_manu";
        if (mysqli_query($this->db_connect, $query)) {
            $view_result = mysqli_query($this->db_connect, $query);
            return $view_result;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    // //function for select info from database

    public function select_info_from_database_manu($data) {
        $query = "select * from tbl_manu where 	d_manu_id='$data'";
        if (mysqli_query($this->db_connect, $query)) {
            $query_data = mysqli_query($this->db_connect, $query);
            return $query_data;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    // updatate function
    public function update_information_manufac() {
        $query = "update tbl_manu set d_manu_name='$_POST[manu_name]' ,d_manu_description='$_POST[manu_description]',d_publication_status='$_POST[publication_status]' where d_manu_id='$_POST[hidden_id]' ";
        if (mysqli_query($this->db_connect, $query)) {
            $result = "Oh congratulation !! Update data successfully";
            return $result;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    // this is for  manufacture published unpublished 

    public function unpublished_manufac_by_id($manufacture_id) {
        $query = "update tbl_manu set d_publication_status='0' where d_manu_id='$manufacture_id' ";
        if (mysqli_query($this->db_connect, $query)) {
            $view_result = "manufacture info unpublished successfully ";
            return $view_result;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    // unpublished

    public function published_manufac_by_id($manufacture_id) {
        $query = "update tbl_manu set d_publication_status='1' where d_manu_id='$manufacture_id' ";
        if (mysqli_query($this->db_connect, $query)) {
            $view_result = "manufacture info published successfully ";
            return $view_result;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    //delete function 
    function delete_manufac_by_id($data) {
        $query = "delete from tbl_manu where d_manu_id='$data' ";
        if (mysqli_query($this->db_connect, $query)) {
            $view_result = "delete information successfully ";
            return $view_result;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    //-------------- End delete_manufacture -----------//
//========================================================================================//
    //-----------------Add Product Info ---------------//


    public function add_product_info($data) {
        $dirctory = "../assets/product_image/";
        $target_file = $dirctory . $_FILES['f_product_image']['name'];
        $file_type = pathinfo($target_file, PATHINFO_EXTENSION);    //file type dekhar jonno 
        $file_size = $_FILES['f_product_image']['size'];
        $image_check = getimagesize($_FILES['f_product_image']['tmp_name']); //image ki na ta janar jonno

        if ($image_check) {

            if (file_exists($target_file)) {
                die('this image is already exists');
            } else {
                if ($file_size > 5242880) {
                    die("file size is too large");
                } else {
                    if ($file_type != "jpg" && $file_type != "png") {
                        die("file type in not valid");
                    } else {
                        //move_uploaded_file($_FILES['f_product_image']['tmp_name'], $target_file);  //ekhane upload deya jay but quer te vule hole ektu problem hoy 
                        $query = "insert into tbl_product(product_name,category_id,d_menu_id,product_price,stock_amount,minimum_stock_amount,product_short_discription,product_long_discription,product_image,publication_status ) "
                                . "values('$data[f_product_name]','$data[category_id]','$data[d_menu_id]','$data[f_product_price]','$data[f_stock_amount]','$data[f_minimum_stock]','$data[f_product_short]','$data[f_product_long]','$target_file','$data[f_publication_status]')";

                        if (mysqli_query($this->db_connect, $query)) {
                            move_uploaded_file($_FILES['f_product_image']['tmp_name'], $target_file);
                            $massage = "product info save successfully ";
                            return $massage;
                        } else {
                            die("Queary problem " . mysqli_error($this->db_connect));
                        }
                    }
                }
            }
        } else {
            die("this uploded file is not an image");
        }
    }

//-------------- End Add Product Info -----------//
//========================================================================================//
//-------------View product info----------------//



    public function view_product() {
        $query = "select p.product_name,p.product_id,p.product_price,p.stock_amount,p.publication_status,c.category_name,m.d_manu_name from tbl_product as p,tbl_category as c,tbl_manu as m where p.category_id=c.	category_id and p.d_menu_id=m.d_manu_id";
        if (mysqli_query($this->db_connect, $query)) {
            $view_result = mysqli_query($this->db_connect, $query);
            return $view_result;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    //-------------- End Add Product Info -----------//
//========================================================================================//
//-------------View all product info----------------//



    public function select_all_product_info($data) {
        $query = "select p.*,c.category_name,m.d_manu_name from tbl_product as p,tbl_category as c,tbl_manu as m where p.category_id=c.category_id and p.d_menu_id=m.d_manu_id and p.product_id='$data'";
        if (mysqli_query($this->db_connect, $query)) {
            $view_result = mysqli_query($this->db_connect, $query);
            return $view_result;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    public function select_all_order_info() {
        $query = "SELECT o.*,c.cusName,p.payment_type,p.payment_status FROM tbl_order as o,customer as c,tbl_payment as p WHERE customer_id=c.cusId and o.order_id=p.order_id";
        if (mysqli_query($this->db_connect, $query)) {
            $view_result = mysqli_query($this->db_connect, $query);
            return $view_result;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    //-------------- End view all Info -----------//
//========================================================================================//
//-------------Unpublished Product by id----------------//



    public function unpublished_product_by_id($producte_id) {
        $query = "update tbl_product set publication_status='0' where product_id='$producte_id' ";
        if (mysqli_query($this->db_connect, $query)) {
            $view_result = "catagori info unpublished successfully ";
            return $view_result;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    //-------------- End Unpublished -----------//
//========================================================================================//
    //-------------published Product by id----------------//


    public function published_product_by_id($producte_id) {
        $query = "update tbl_product set publication_status='1' where product_id='$producte_id' ";
        if (mysqli_query($this->db_connect, $query)) {
            $view_result = "catagori info published successfully ";
            return $view_result;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    //-------------- End Unpublished -----------//
//========================================================================================//
    //------------delete_product_by_id -----------------//

    function delete_product_by_id($data) {
        $query = "delete from tbl_product where product_id='$data' ";
        if (mysqli_query($this->db_connect, $query)) {
            $view_result = "Delete information successfully ";
            return $view_result;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

//-----------------------------------order------------------------
    public function select_customer_info_By_order_id($orderId) {
        
        $query = "SELECT o.order_id,o.customer_id,c.* FROM tbl_order as o,customer as c   WHERE customer_id=c.cusId and o.order_id='$orderId'";
        if (mysqli_query($this->db_connect, $query)) {
            $query_data = mysqli_query($this->db_connect, $query);
            return $query_data;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

     public function select_shipping_info_By_order_id($orderId) {
        
        $query = "SELECT o.order_id,o.shipping_id,s.* FROM tbl_order as o,tbl_shipping as s    WHERE o.shipping_id=s.shi_id and o.order_id='$orderId'";
        if (mysqli_query($this->db_connect, $query)) {
            $query_data = mysqli_query($this->db_connect, $query);
            return $query_data;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }
     public function select_payment_info_By_order_id($orderId) {
        
        $query = "SELECT o.order_id,p.* FROM tbl_order as o,tbl_payment as p   WHERE o.order_id=p.order_id and o.order_id='$orderId'";
        if (mysqli_query($this->db_connect, $query)) {
            $query_data = mysqli_query($this->db_connect, $query);
            return $query_data;
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }
    // ========================================================================
    // =========================================================================
    public function logout() {

        unset($_SESSION['author_name']); //session take unset kore deya holo
        unset($_SESSION['author_id']);

        header('location:index.php');
    }

}
