<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="text-center text-success">Order Completed Successfully!</h2><br>
                    <h4>Thank you! Your purchase is complete and you will receive a confirmation email shortly.</h4><br>
                    Go to HomePage <a href="index.php"> Click here</a>

                </div>
            </div>
        </div>
    </div>
</div>