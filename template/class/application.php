<?php

/**
 * @author rumman
 */
class Application {

    private $db_connect;

    public function __construct() {
        // just connect database 
        $host = 'localhost';
        $user = 'root';
        $password = '';
        $database = 'template';
        $this->db_connect = mysqli_connect($host, $user, $password, $database);
        if (!$this->db_connect) {
            die("Database Not Connect Successfully !!" . mysqli_error($this->db_connect));
        }
    }

    public function select_all_published_category() {
        $query = "select * from tbl_category where publication_status='1'";
        if (mysqli_query($this->db_connect, $query)) {
            $result = mysqli_query($this->db_connect, $query);
            return $result;
        } else {
            die(" Query Problem " . mysqli_error($this->db_connect));
        }
    }

    public function select_all_published_manufacture() {
        $query = "select * from tbl_manu where d_publication_status='1'";
        if (mysqli_query($this->db_connect, $query)) {
            $result = mysqli_query($this->db_connect, $query);
            return $result;
        } else {
            die(" Query Problem " . mysqli_error($this->db_connect));
        }
    }

    //=================================================================================


    public function select_larest_product() {
        $query = "select * from tbl_product where publication_status='1' order by product_id DESC LIMIT 6";
        if (mysqli_query($this->db_connect, $query)) {
            $result = mysqli_query($this->db_connect, $query);
            return $result;
        } else {
            die(" Query Problem " . mysqli_error($this->db_connect));
        }
    }

//==============================================================================================
    public function select_product_info_by_id($product_id) {
        $query = "select p.*,c.	category_name,m.d_manu_name from tbl_product as p,tbl_category as c,tbl_manu as m where p.category_id =c.category_id and p.d_menu_id=m.d_manu_id and p.product_id='$product_id' ";
        if (mysqli_query($this->db_connect, $query)) {
            $result = mysqli_query($this->db_connect, $query);
            return $result;
        } else {
            die(" Query Problem " . mysqli_error($this->db_connect));
        }
    }

    //====================================================================

    public function add_to_cart($data) {
        $product_i = $data['product_id'];

        $query = "select product_name,product_price,product_image from tbl_product where product_id='$product_i' ";
        $result = mysqli_query($this->db_connect, $query);
        $product_info = mysqli_fetch_assoc($result);

        session_start();
        $session_id = session_id();

        $query = "insert into tbl_tem_cart(session_id,product_id,product_name,product_price,product_quentiity,product_image) values('$session_id','$product_i','$product_info[product_name]','$product_info[product_price]','$data[fproduct_quentiity]','$product_info[product_image]') ";
        mysqli_query($this->db_connect, $query);
        header('location:cart.php');
    }

    function select_cart_product_by_session_id($session) {
        $query = "select * from tbl_tem_cart where session_id='$session'";
        if (mysqli_query($this->db_connect, $query)) {
            $result = mysqli_query($this->db_connect, $query);
            return $result;
        } else {
            die(" Query Problem " . mysqli_error($this->db_connect));
        }
    }

    //================================================================
    function update_cart_product($data) {
        if ($data['product_quentiity'] > 0) {

            $query = "update tbl_tem_cart set product_quentiity='$data[product_quentiity]' where tem_id='$data[tem_id]' ";
            if (mysqli_query($this->db_connect, $query)) {
                $result = "cart Quentity update successfully";
                return $result;
            } else {
                die(" Query Problem " . mysqli_error($this->db_connect));
            }
        } else {
            $result = "Please input a valid integer . Not 0 or negetive";
            return $result;
        }
    }

    //====================================================================================

    function delete_cart_product($cart_id) {
        $query = "delete from tbl_tem_cart  where tem_id='$cart_id' ";
        if (mysqli_query($this->db_connect, $query)) {
            $result = "cart Quentity Delete successfully";
            return $result;
        } else {
            die(" Query Problem " . mysqli_error($this->db_connect));
        }
    }

    public function select_product_info_by_category_id($cart_id) {
        $query = "select * from tbl_product where category_id='$cart_id' and publication_status=1 ORDER BY product_id DESC ";
        if (mysqli_query($this->db_connect, $query)) {
            $result = mysqli_query($this->db_connect, $query);
            return $result;
        } else {
            die(" Query Problem " . mysqli_error($this->db_connect));
        }
    }

    public function save_customer_info($data) {
        $query = "insert into customer(cusName,cusEmail,cusPassword,cusContact,cusAddress,cusCity,cusDistrict) values('$data[firstName]','$data[email]','$data[password]','$data[contact]','$data[address]','$data[city]','$data[district]')";
        if (mysqli_query($this->db_connect, $query)) {
            $customerId = mysqli_insert_id($this->db_connect);
            
            $_SESSION['customerId'] = $customerId;
            $_SESSION['customerName'] = $data['firstName'];
            
            //mail
            $to=$data['email'];
            $decustomerId= base64_encode($customerId);
            $subject="customer Email Varificaton";
            $message="<span> thanks for join our Community</span><br>"
                    . "<span> your login information goes here</span><br>"
                    . "<a href='http://localhost/template/updateMail.php?id=$decustomerId'>active Account</a>";
            $header='Form:info@rumman.com';
            mail($to, $subject, $message,$header);
            echo $message;
            exit();
         //   header('Location:shipping.php');
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    public function select_customer_info($custimerId) {
        $query = "select * from customer where cusId='$custimerId' ";
        if (mysqli_query($this->db_connect, $query)) {
            $result = mysqli_query($this->db_connect, $query);
            return $result;
        } else {
            die(" Query Problem " . mysqli_error($this->db_connect));
        }
    }

    public function save_shipping_info($data) {
        $query = "insert into tbl_shipping(shi_Name,shi_Email,shi_contact,shi_address,shi_city,shi_district) values('$data[firstName]','$data[email]','$data[contact]','$data[address]','$data[city]','$data[district]')";
        if (mysqli_query($this->db_connect, $query)) {
            $shippingId = mysqli_insert_id($this->db_connect);
            session_start();
            $_SESSION['shippingId'] = $shippingId;
            header('Location:payment.php');
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    public function save_order_info($data) {
        $payment_type = $data['payment_type'];
        if ($payment_type == 'cashonDelivery') {
            session_start();
            $shippingId = $_SESSION['shippingId'];
            $coustomer_id = $_SESSION['customerId'];
            $total = $_SESSION['orderTotal'];

            $query = "insert into tbl_order(customer_id,shipping_id,order_total) values('$coustomer_id','$shippingId','$total')";

            if (mysqli_query($this->db_connect, $query)) {
                $order_ID = mysqli_insert_id($this->db_connect);

                $query = "insert into tbl_payment(order_id,payment_type) values('$order_ID','$payment_type')";
                if (mysqli_query($this->db_connect, $query)) {

                    $session_id = session_id();

                    $query = "SELECT * FROM tbl_tem_cart where session_id='$session_id'";
                    $result = mysqli_query($this->db_connect, $query);
                    while ($productInfo = mysqli_fetch_assoc($result)) {
                        $query = "insert into tbl_order_details(order_id,product_id,product_name,product_price,product_quentity,product_image) values('$order_ID','$productInfo[product_id]','$productInfo[product_name]','$productInfo[product_price]','$productInfo[product_quentiity]','$productInfo[product_image]')";
                        mysqli_query($this->db_connect, $query);
                    }
                    $query = ("DELETE FROM tbl_tem_cart WHERE  session_id='$session_id'");
                    mysqli_query($this->db_connect, $query);
                    unset($_SESSION['orderTotal']);
                    header('Location:customer_home.php');
                } else {
                    die("Query Problem" . mysqli_error($this->db_connect));
                }
            } else {
                die("Query Problem" . mysqli_error($this->db_connect));
            }
        }
    }

    
    public function update_customer_status($id){
          $query = "update customer set activation_status=1 where cusId='$id' ";
        if (mysqli_query($this->db_connect, $query)) {
            header('location:shipping.php');
        } else {
            die("Query Problem" . mysqli_error($this->db_connect));
        }
    }

    public function customer_logout() {
        unset($_SESSION['customerId']);
        unset($_SESSION['shippingId']);

        unset($_SESSION['customerName']);

        header('Location:index.php');
    }
    
    
    public function customer_email_check($email){
         $query = "select * from customer where cusEmail='$email' ";
        if (mysqli_query($this->db_connect, $query)) {
            $result = mysqli_query($this->db_connect, $query);
            return $result;
        } else {
            die(" Query Problem " . mysqli_error($this->db_connect));
        }
    }

    // final end point
}
